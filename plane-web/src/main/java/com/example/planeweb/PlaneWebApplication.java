package com.example.planeweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlaneWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaneWebApplication.class, args);
    }

}
