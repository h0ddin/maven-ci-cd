pipeline {
    agent any

    options {
        skipDefaultCheckout(true)
    }

    tools {
        maven 'maven3_8_4'
        jdk 'jdk8u322'
    }

    stages {
        stage('Init') {
            steps {
                checkout scm
            }
        }

        stage('Build') {
            steps {
                echo "Building ${env.JOB_NAME}"
                sh 'mvn clean install'
            }
        }
    }
}