pipeline {
    agent any

    options {
        skipDefaultCheckout(true)
    }

    tools {
        maven 'maven3_8_4'
        jdk 'jdk8u322'
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }

        stage('Build') {
            steps {
                echo "Building ${env.JOB_NAME}"
                echo "LETS GO"
                sh 'mvn clean install'
            }
        }

        stage('Test') {
            steps {
                script {
                    // Preparing Git
                    sh 'git fetch'
                    sh "git checkout ${env.BRANCH_NAME}"
                    sh "git pull"
                    sh "git config user.email \"Mariusz\""
                    sh "git config user.name \"maniuchh@gmail.com\""
                    sh 'git status'
                    sh "mvn build-helper:parse-version versions:set -DnewVersion='\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.incrementalVersion}' versions:commit"
                    def version = sh script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true
                    echo 'version:'
                    echo version
                    sh 'git add .'
                    sh "git commit -m '[JENKINS] Prepare new release version ${version}'"
                    sh 'git push'

                    //create branch release/version
                    sh "git checkout -b release/${version}"
                    sh "git push --set-upstream origin release/${version}"
                    //create tag
                    sh "git tag ${version} -a -m 'release/${version}'"
                    sh "git push origin ${version}"
                    //and push both

                    sh "git checkout ${env.BRANCH_NAME}"
                    sh "mvn build-helper:parse-version versions:set -DnewVersion='\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion}-SNAPSHOT' versions:commit"
                    def newVersion = sh script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true
                    echo 'newVersion:'
                    echo newVersion
                    sh 'git add .'
                    sh "git commit -m '[JENKINS] Prepare new snapshot version ${newVersion}'"
                    sh 'git push'
                }
            }
        }
    }
}